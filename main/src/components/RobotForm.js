import React, { Component } from 'react'

class RobotForm extends Component {
    
    constructor(props){
        super(props)
        this.state={
            name:"",
            type:"worker",
            mass:0
        }
    }
    
    render(){
        return(
            <div>
                 Add Robot
                 <input id="name" type="text" value={this.state.name} placeholder="Nume" onChange={(e)=>{
                    this.setState({
                        name:e.target.value
                    }) 
                 }}></input>
                 <select id="type" value={this.state.type} placeholder="Tip" onChange={(e)=>{
                    this.setState({
                        type:e.target.value
                    }) 
                 }}>
                    <option>worker</option>
                    <option>non-worker</option>
                 </select>
                 <input id="mass" type="number" value={this.state.mass} placeholder="Masa" onChange={(e)=>{
                    this.setState({
                        mass:e.target.value
                    }) 
                 }}></input>
                 <button onClick={(e)=>{
                    const robot=this.state
                    this.props.onAdd(robot)
                 }} value="add">Add</button>
            </div>
            );
    }
}

export default RobotForm